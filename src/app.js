import React from "react";
import {Helmet} from "react-helmet";
import MainContentWrapper from './content/main-content-wrapper';
const App = () => {
    return (
        <div className="application-wrapper">
            <Helmet>
                <title>basic-okta</title>
                <meta charSet="utf-8"/>
                <meta http-equiv="Pragma" content="no-cache"/>
                <meta http-equiv="Expires" content="0"/>
            </Helmet>
            <MainContentWrapper />
        </div>
    );
};

export default App;