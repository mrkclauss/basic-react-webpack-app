import React from 'react';
import {BrowserRouter as Router, Route, NavLink, Redirect} from 'react-router-dom';
import {Security, LoginCallback, SecureRoute } from '@okta/okta-react';
import {OktaAuth} from '@okta/okta-auth-js';
import {PublicPage, HomePage, AboutPage} from './index';

const CALLBACK_PATH = '/home';

const config = {
    clientId: '0oa1khlk7tdGflm4G4x7',
    issuer: 'https://dev-270131.okta.com/oauth2/default',
    redirectUri: window.location.origin + CALLBACK_PATH
};

const oktaAuth = new OktaAuth(config);

const TestPage = (props) => {
    return (
        <Router>
            <Security oktaAuth={oktaAuth}>
        <div>
                <nav>
                    <NavLink to='/public'>Public</NavLink>
                    <NavLink to='/about'>About</NavLink>
                    <NavLink to='/home'>Home</NavLink>
                </nav>
                <Route path={CALLBACK_PATH} component={LoginCallback} />
                <Route path="/public" component={PublicPage} />
                <Route path="/about" component={AboutPage} />
                <SecureRoute path="/home" component={HomePage}/>
        </div>
        </Security>
        </Router>
    );
};

export default TestPage;