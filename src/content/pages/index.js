import PublicPage from './public-page';
import HomePage from './home-page';
import AboutPage from './about-page';



export {
    PublicPage,
    HomePage,
    AboutPage
}