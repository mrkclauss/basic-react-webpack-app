import React from 'react';
import LoadingOverlay from 'react-loading-overlay';
import {setData_cy} from "../utils/gen-utils";
import TestPage from "./pages/test-page";

export default props => {

        return (
            <LoadingOverlay
                active={false}
                spinner
                text={c_data.data.main_content_overlay.text}
            >
                <div
                    className={c_data.classNames.main_content_wrapper}
                    data-cy={c_data.cy.main_content_wrapper}
                >
                    <TestPage />
                </div>
            </LoadingOverlay>
        )

}

export const c_data = {
    cy: {
        main_content_wrapper: setData_cy('main_content_wrapper'),
        main_content_overlay: setData_cy('main_content_overlay'),
    },
    classNames: {
        main_content_wrapper: 'main-content-wrapper',
        main_content_overlay: 'main-content-overlay'
    },
    data: {
        main_content_overlay: {
            text: 'Overlay Text'
        }
    }
};